/*   https://quentinchap.github.io/f2-2018/#/   */
/*   https://punkapi.com/documentation/v2  */
/*   https://material-ui.com/demos/cards/   */
/*   https://quentinchap.github.io/f2-2018/react1#/27  */

import React, { Component } from 'react';
import { Route } from 'react-router';
import './App.css';
import Baniere from './Components/Items/Baniere';
import Home from './Components/Pages/home'
import NewInstaPost from './Components/Pages/newInstaPost'
import confirmationEnvoi from './Components/Pages/confirmationEnvoi'
import {BrowserRouter as Router} from 'react-router-dom'

const API = 'https://api.punkapi.com/v2/';
const DEFAULT_QUERY = 'beers';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      beers: [],
    };
  }

  componentDidMount() {
    fetch(API + DEFAULT_QUERY)
      .then(response => response.json())
      .then(data => this.setState({ beers: data }));
      /*.then(response => this.setState({beers : response.json().data}));*/
  }
  

  render() {

    //const { beers } = this.state;

    return (
      <Router>
        <div className="App">    
          <Baniere className="App-baniere" user="user"/>

        
          <div>
            <Route path="/" exact component={Home} />
            <Route path="/newInstaPost" component={NewInstaPost} />
            <Route path="/confirmationEnvoi" component={confirmationEnvoi} />
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
