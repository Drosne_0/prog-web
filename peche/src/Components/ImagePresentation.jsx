import React from 'react';
import GrosBurger from '../Resources/GrosBurger.jpeg';
import Truffade from '../Resources/Truffade.jpeg';

export default class ImagePresentation extends React.Component {
    constructor(props) { 
        super(props);
      }


      render() {

        return (
            <div>
           <img src={GrosBurger} class="App-ImageBurger"/>
                    
           <img src={Truffade} class="App-ImageTruffade"/>
        </div>
        );
        } 
  }