import './Pages.css'
import React, { Component } from "react";
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';

export default class NewInstaPost extends Component {

    render() {
        return (
            <div>
                <div>
                    Votre Post a bien été envoyé (enfin en théorie)
                </div>
                <div>
                    <Link to='/'>
                        <Button>
                            Retour à l'acceuil
                        </Button>
                    </Link>
                </div>
            </div>
        )
    }

}