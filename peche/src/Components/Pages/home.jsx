import React, { Component } from "react";
import '../../App.css';
import Post from '../Post'
import Hello from '../Hello'

const API = 'https://api.punkapi.com/v2/';
const DEFAULT_QUERY = 'beers';



export default class Home extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          beers: [],
        };
      }
    
      componentDidMount() {
        fetch(API + DEFAULT_QUERY)
          .then(response => response.json())
          .then(data => this.setState({ beers: data }));
          /*.then(response => this.setState({beers : response.json().data}));*/
      }
      
    
      render() {
    
        const { beers } = this.state;
    
        return (
          <div className="App">    
            
        <header className="App-header">
          
          <Hello user="user"/>

        </header>
            <ul>
              {beers.map(beer =>
                <li key={beer.id}>
                  <a href={beer.image_url}>{beer.name}</a>
                </li>
              )}
            </ul>
            <Post className="App-Post" user="Denys" img="https://images.punkapi.com/v2/18.png" description="Description de merde" date="Date"/>
          </div>
        );
      }
  }