import './Pages.css'
import React, { Component } from "react";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import { Link } from 'react-router-dom';
import Upload from 'material-ui-upload/Upload'

export default class NewInstaPost extends Component {
  state = {
    value: 'hidden',//Cache ou non les éléments séléctionnés
    pret: 'false',//Donne si l'URL n'est pas vide ou si un fixhier a été choisi
    URL: "",
    Description: ""
  };

  handleChange = event => {
    this.setState({ value: event.target.value });
  };

  onFileLoad = (e, file) => {
    this.setState({pret: "true"});
    console.log(e.target.result, file.name);
  }

  publier = event => {
    if(this.state.value === "URL") {
      fetch(process.env.BACKENDURL + '/newInstaPost/', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          user: "Moi",
          date: new Date().getDate,
          description: this.state.Description,
          img: URL,
        })
      })
    }
    else if (this.state.value === "Fichier") {

    }


  }

  render() {
    console.log(this.state);
    return (
      <div id="contact" className="NewPost">
        <header className="NewPost-Header">
          Nouveau Poste
        </header>
        <div className="NewPost-body">
          <div>
            <TextField 
            id="description"
            multiline
            onChange={e => this.setState({ Description: e.target.value })}
            rows="6"
            label="Description"
            helperText="Têxte censé être important"/>
            
          </div>
          <br/><br/><br/>

          <FormControl component="fieldset">
            <FormLabel component="legend">Importer un fichier</FormLabel>
            <RadioGroup
              aria-label="Importer un fichier"
              name="import1"
              value={this.state.value}
              
            >
              <FormControlLabel value='URL' control={<Radio />} onChange={this.handleChange} label="URL" />
              <TextField id="URL"
                fullWidth
                onChange={e => this.setState({ URL: e.target.value, pret: "true" })}
                itemRef="URLTextField"
                helperText="Copier l'URL"
                style={{display: (this.state.value === "URL")? "block":"none"  }}
                />
              <FormControlLabel value='Fichier' control={<Radio />} onChange={this.handleChange} label="Selectionner un fichier" />
              <Upload
              style={{ margin: 50, display: (this.state.value === "Fichier")? "block":"none"  }}
              onFileLoad={e => this.setState({ pret: "true" })}
              label="Add File"
              >
                
              </Upload>
            </RadioGroup>
        </FormControl><br/><br/><br/>
        <Link to='/confirmationEnvoi'>
          <Button variant="contained" color="secondary" onClick={this.publier}
          style={{ margin: 50, display: (this.state.pret === "true")? "block":"none"  }}>
            Publier
          </Button>
        </Link>
        </div>

      </div>
    );
  }
}